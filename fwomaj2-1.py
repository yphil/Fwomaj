#!/usr/bin/env python3

from video_player import VideoPlayer

import sys

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gio, Gtk

class AppWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # This will be in the windows group and have the "win" prefix
        max_action = Gio.SimpleAction.new_stateful(
            "maximize", None, GLib.Variant.new_boolean(False)
        )
        max_action.connect("change-state", self.on_maximize_toggle)
        self.add_action(max_action)

        # Keep it in sync with the actual state
        self.connect(
            "notify::is-maximized",
            lambda obj, pspec: max_action.set_state(
                GLib.Variant.new_boolean(obj.props.is_maximized)
            ),
        )

        self.connect("realize", self.__on_realize)

        lbl_variant = GLib.Variant.new_string("String 1")
        lbl_action = Gio.SimpleAction.new_stateful(
            "change_label", lbl_variant.get_type(), lbl_variant
        )
        lbl_action.connect("change-state", self.on_change_label_state)
        self.add_action(lbl_action)

        self.label = Gtk.Label(label=lbl_variant.get_string(), margin=30)

        self.paned = Gtk.Paned(orientation=Gtk.Orientation.VERTICAL)

        self.main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        self.button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL)

        self.playwin = Gtk.Box()
        listwin = Gtk.Box()

        # print(dir(playwin.props))

        self.paned.add1(self.playwin)
        self.paned.add2(listwin)

        button1 = Gtk.Button(label="Button1")
        button2 = Gtk.Button(label="Button2")
        button3 = Gtk.Button(label="Play")
        button4 = Gtk.Button(label="Stop")

        self.button_box.pack_start(button3, False, False, 0)
        self.button_box.pack_start(button4, False, False, 0)

        # self.playwin.pack_start(button1, False, False, 0)
        listwin.pack_start(button2, True, True, 0)

        self.main_box.add(self.paned)
        # self.main_box.add(self.paned, True, True, 0)

        self.main_box.add(self.button_box)
        
        self.add(self.main_box)

        # self.paned.show()

        if self.get_realized():
            print("realed")
        


    def __on_realize(self, widget: Gtk.Window, data=None) -> None:
        print("realing")
        window = widget.get_window()
        self.__xid = window.get_xid()
        self.video_player = VideoPlayer("tmp/examples_data_sintel-trailer-480p.webm", loop=True, xid=self.__xid, box=self.playwin)
        # self.build_ui()
        print("xid: ", self.__xid)
        # self.playwin.pack_start(self.video_player, True, True, 0)
        self.playwin.add(self.video_player)
        # self.main_box.add(self.video_player)
        # self.realized = True
        self.video_player.play()
        self.show_all()

    def on_change_label_state(self, action, value):
        action.set_state(value)
        self.label.set_text(value.get_string())

    def on_maximize_toggle(self, action, value):
        action.set_state(value)
        if value.get_boolean():
            self.maximize()
        else:
            self.unmaximize()

class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="org.example.myapp",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE,
            **kwargs
        )
        self.window = None

        self.add_main_option(
            "test",
            ord("t"),
            GLib.OptionFlags.NONE,
            GLib.OptionArg.NONE,
            "Command line test",
            None,
        )

    def do_startup(self):
        print("do_startup: ")
        Gtk.Application.do_startup(self)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", self.on_quit)
        self.add_action(action)

        builder = Gtk.Builder()
        builder.add_from_file("menu.glade")
        # self.set_app_menu(builder.get_object("app-menu"))
        # builder.add_objects_from_file("menu.glade", ("button1", "button2"))

        self.set_app_menu(builder.get_object("app-menu"))


        
        # menu.show_all()
        
    def do_activate(self):
        print("do_activate: ")
        # We only allow a single window and raise any existing ones
        if not self.window:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.window = AppWindow(application=self, title="Main Window")

        self.window.present()

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()

        if "test" in options:
            # This is printed on the main instance
            print("Test argument recieved: %s" % options["test"])

        self.activate()
        return 0

    def on_about(self, action, param):
        about_dialog = Gtk.AboutDialog(transient_for=self.window, modal=True)
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()


if __name__ == "__main__":
    app = Application()
    app.run(sys.argv)
