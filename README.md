## Video Cheese Cutter

[![pipeline status](https://framagit.org/yphil/fwomaj/badges/master/pipeline.svg)](https://framagit.org/yphil/fwomaj/-/pipelines)
[![License GPLv3](https://img.shields.io/badge/license-GPL_v3-green.svg)](http://www.gnu.org/licenses/gpl-3.0.html)
[![Liberapay](https://img.shields.io/liberapay/receives/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Liberapay](https://img.shields.io/liberapay/goal/yPhil?logo=liberapay)](https://liberapay.com/yPhil/donate)
[![Ko-Fi](https://img.shields.io/badge/Ko--fi-F16061?label=buy+me+a&style=flat&logo=ko-fi&logoColor=white)](https://ko-fi.com/yphil/tiers)

Fwomaj is a small media player to quickly view and edit video files (it's called "derushing") for later editing.
Change framerate and other encoding parameters, trim the start and end, etc.

**It is under heavy development at the moment, undergoing a total rehaul, so please symply do not use it**.

![screenshot](https://framagit.org/yphil/Fwomaj/raw/master/screenshot.jpg)

## Usage
Fwomaj can set and save the following parameters to a *new* (in the current directory, named `FILE_NAME_START-END_FRAMERATE.EXTENSION` ie `my_huge_video.mp4_0:02:45-0:02:52_60fps.mkv`) video *file*:

- Start time
- End time
- Framerate
- Codec container

Move the **time sliders** to set the *START* and *END* of the new movie clip ; Use the **Encoding** menu to select the **framerate** (default *30fps*) and  **container** (default *Matroska*). Upon clicking the **Save** (disk icon) button, the trimmed and encoded file is saved in the original file directory.

## Install
Run [build-install.sh](https://framagit.org/yphil/Fwomaj/blob/master/build-install.sh) or [download the latest package](https://framagit.org/yphil/Fwomaj/-/archive/master/Fwomaj-master.zip).


Icon by [Buuf](http://mattahan.deviantart.com)

Brought to you with 💚 by [Yassin Philip](http://yphil.bitbucket.io) | Help me make it 🍎  [Donate](https://liberapay.com/yPhil/donate)
