#!/usr/bin/env python3.10

# apt install libges-1.0-dev gstreamer1.0-plugins-bad

import sys
import os
import argparse
import gi
import datetime
import time
gi.require_version('Gst', '1.0')
gi.require_version('Gtk', '3.0')
gi.require_version('Gdk', '3.0')
gi.require_version('GstVideo', '1.0')
gi.require_version('GstAudio', '1.0')
gi.require_version('GES', '1.0')
gi.require_version('GstPbutils', '1.0')
from gi.repository import GLib
from gi.repository import Gdk
from gi.repository import GstVideo
from gi.repository import GstAudio
from gi.repository import GstPbutils
from gi.repository import Gtk
from gi.repository import Gst
from gi.repository import GES
from gi.repository import GObject

__version__ = '1.1.1'

class Fwomaj(object):

    def __init__(self):
        
        # initialize GTK
        Gtk.init(sys.argv)

        # initialize GStreamer
        Gst.init(sys.argv)

        GES.init()

        self.seconds_to_seek = 5
        self.state = Gst.State.NULL
        self.duration = Gst.CLOCK_TIME_NONE
        self.playbin = Gst.ElementFactory.make("playbin", "playbin")
        if not self.playbin:
            print("ERROR: Could not create playbin.")
            sys.exit(1)

        parser = argparse.ArgumentParser()
        
        parser.add_argument('files',
                            nargs='+',
                            help='File(s) PATH to open and play')
        parser.add_argument('-v', '--version', action='version', version='%(prog)s {version}'.format(version=__version__))
        
        args = parser.parse_args()

        if args.files:
            self.playbin.set_property("uri", "file://" + args.files[0])
        else:
            pass
        # self.playbin.set_property("uri", "file://" + sys.argv[1:])
        # sys.exit(1)

        def convert(list):
            return tuple(list)

        self.dummyArray = []

        if args.files:
            self.playlist = args.files
            lines = [line.rstrip() for line in args.files]
        else:
            if os.path.exists("playlist.txt"):

                with open("playlist.txt") as f:
                    lines = [line.rstrip() for line in f]

            else:
                lines = [line.rstrip() for line in args.files]
                
        for i in range(len(lines)):
            self.dummyArray.append(convert([i, lines[i]]))

        print('dummyArray: ', self.dummyArray)

        # connect to interesting signals in playbin
        self.playbin.connect("video-tags-changed", self.on_tags_changed)
        self.playbin.connect("audio-tags-changed", self.on_tags_changed)
        self.playbin.connect("text-tags-changed", self.on_tags_changed)

        # create the GUI
        self.build_ui()

        # instruct the bus to emit signals for each received message
        # and connect to the interesting signals
        bus = self.playbin.get_bus()
        bus.add_signal_watch()
        bus.connect("message::error", self.on_error)
        bus.connect("message::eos", self.on_eos)
        bus.connect("message::state-changed", self.on_state_changed)
        bus.connect("message::application", self.on_application_message)
        
    # set the playbin to PLAYING (start playback), register refresh callback
    # and start the GTK main loop
    def start(self):
        # start playing
        ret = self.playbin.set_state(Gst.State.PLAYING)
        if ret == Gst.StateChangeReturn.FAILURE:
            print("ERROR: Unable to set the pipeline to the playing state")
            sys.exit(1)
        else:
            self.treeview.row_activated(Gtk.TreePath(0), Gtk.TreeViewColumn(None))
            self.treeview.set_cursor(Gtk.TreePath(0))
            
        self.playbin.set_volume(GstAudio.StreamVolumeFormat.LINEAR, 0.9)

        # register a function that GLib will call every second
        GLib.timeout_add_seconds(1, self.refresh_ui)

        # start the GTK main loop. we will not regain control until
        # Gtk.main_quit() is called
        Gtk.main()
        
        # free resources
        self.cleanup()

    # set the playbin state to NULL and remove the reference to it
    def cleanup(self):
        if self.playbin:
            self.playbin.set_state(Gst.State.NULL)
            self.playbin = None

    def build_ui(self):
        main_window = Gtk.Window(type=Gtk.WindowType.TOPLEVEL)
        main_window.connect("delete-event", self.on_delete_event)
        
        video_window = Gtk.DrawingArea.new()
        # video_window.set_double_buffered(False)
        video_window.connect("realize", self.on_realize)
        video_window.connect("draw", self.on_draw)

        # MENU:

        video_window.set_events(Gdk.EventMask.BUTTON_PRESS_MASK |
                                Gdk.EventMask.KEY_PRESS_MASK)

        grid = Gtk.Grid()

        accelgroup = Gtk.AccelGroup()
        main_window.add_accel_group(accelgroup)
        
        menubar = Gtk.MenuBar()
        menubar.set_hexpand(True)
        grid.attach(menubar, 0, 0, 1, 1)
        
        file_menu = Gtk.Menu()
        file_menuitem = Gtk.MenuItem(label="File")
        recent_menuitem = Gtk.MenuItem(label="Recent Items")
        recent_chooser_menu = Gtk.RecentChooserMenu()
        recent_chooser_menu.connect('item-activated', self.on_item_activated)
        recent_menuitem.set_submenu(recent_chooser_menu)
        
        file_menuitem.set_submenu(file_menu)
        menuitem_open = Gtk.MenuItem(label="Open")
        menuitem_playlist = Gtk.CheckMenuItem(label="Playlist")
        menuitem_quit = Gtk.MenuItem(label="Quit")
        menuitem_properties = Gtk.CheckMenuItem(label="Properties")
        menuitem_info = Gtk.MenuItem(label="Info")
        menuitem_quit.connect("activate", self.on_delete_event, "activate")
        menuitem_properties.connect("activate", self.on_properties, "activate")
        menuitem_playlist.connect("activate", self.on_toggle_playlist, "activate")
        menuitem_info.connect("activate", self.on_info)

        menuitem_open.add_accelerator("activate",
                                      accelgroup,
                                      Gdk.keyval_from_name("o"),
                                      Gdk.ModifierType.CONTROL_MASK,
                                      Gtk.AccelFlags.VISIBLE)

        menuitem_playlist.add_accelerator("activate",
                                          accelgroup,
                                          Gdk.keyval_from_name("l"),
                                          0,
                                          Gtk.AccelFlags.VISIBLE)

        menuitem_info.add_accelerator("activate",
                                      accelgroup,
                                      Gdk.keyval_from_name("i"),
                                      Gdk.ModifierType.CONTROL_MASK,
                                      Gtk.AccelFlags.VISIBLE)

        menuitem_properties.add_accelerator("activate",
                                      accelgroup,
                                      Gdk.keyval_from_name("p"),
                                      0,
                                      Gtk.AccelFlags.VISIBLE)

        menuitem_quit.add_accelerator("activate",
                                      accelgroup,
                                      Gdk.keyval_from_name("q"),
                                      Gdk.ModifierType.CONTROL_MASK,
                                      Gtk.AccelFlags.VISIBLE)
        
        menuitem_open.connect("activate", self.on_file_open)
        # menuitem_open.connect("activate", self.on_seek, "right", self.seconds_to_seek)

        file_menu.append(menuitem_open)
        file_menu.append(recent_menuitem)
        file_menu.append(menuitem_properties)
        file_menu.append(menuitem_playlist)
        file_menu.append(menuitem_info)
        file_menu.append(menuitem_quit)
        
        play_menu = Gtk.Menu()
        play_menuitem = Gtk.MenuItem(label="Play")
        play_menuitem.set_submenu(play_menu)

        menuitem_toggle_play = Gtk.MenuItem(label="Play / Pause")
        menuitem_toggle_play.add_accelerator("activate",
                                             accelgroup,
                                             Gdk.keyval_from_name("space"),
                                             0,
                                             Gtk.AccelFlags.VISIBLE)
        menuitem_toggle_play.connect("activate", self.on_toggle_play_event, "activate")

        menuitem_seek_left = Gtk.MenuItem(label="- " + str(self.seconds_to_seek) + "Seconds")
        menuitem_seek_left.add_accelerator("activate",
                                           accelgroup,
                                           Gdk.keyval_from_name("Left"),
                                           0,
                                           Gtk.AccelFlags.VISIBLE)
        menuitem_seek_left.connect("activate", self.on_seek, "left", self.seconds_to_seek)
                
        menuitem_seek_right = Gtk.MenuItem(label="+ " + str(self.seconds_to_seek) + "Seconds")
        menuitem_seek_right.add_accelerator("activate",
                                            accelgroup,
                                            65363,
                                            0,
                                            Gtk.AccelFlags.VISIBLE)
        menuitem_seek_right.connect("activate", self.on_seek, "right", self.seconds_to_seek)
        
        menuitem_seek_left_big = Gtk.MenuItem(label="- " + str(self.seconds_to_seek) + "Minutes")
        menuitem_seek_left_big.add_accelerator("activate",
                                               accelgroup,
                                               Gdk.KEY_Page_Down,
                                               0,
                                               Gtk.AccelFlags.VISIBLE)
        # menuitem_seek_left_big.connect("activate", self.on_seek, "left", 900)
                
        menuitem_seek_right_big = Gtk.MenuItem(label="+ " + str(self.seconds_to_seek) + "Minutes")
        menuitem_seek_right_big.add_accelerator("activate",
                                                accelgroup,
                                                Gdk.KEY_Page_Up,
                                                0,
                                                Gtk.AccelFlags.VISIBLE)

        play_menu.append(menuitem_toggle_play)
        play_menu.append(menuitem_seek_left)
        play_menu.append(menuitem_seek_right)
        play_menu.append(menuitem_seek_left_big)
        play_menu.append(menuitem_seek_right_big)
        
        edit_menu = Gtk.Menu()
        edit_menuitem = Gtk.MenuItem(label="Edit")
        edit_menuitem.set_submenu(edit_menu)
        menuitem_toggle_cut = Gtk.CheckMenuItem(label="Cut / Trim")
        menuitem_toggle_cut.connect("activate", self.on_edit_menu_trim, "activate")
        menuitem_toggle_cut.add_accelerator("activate",
                                            accelgroup,
                                            Gdk.keyval_from_name("t"),
                                            Gdk.ModifierType.CONTROL_MASK,
                                            Gtk.AccelFlags.VISIBLE)
        edit_menu.append(menuitem_toggle_cut)

        help_menu = Gtk.Menu()
        help_menuitem = Gtk.MenuItem(label="Help")
        help_menuitem.set_submenu(help_menu)
        menuitem_about = Gtk.MenuItem(label="About Fwomaj")
        menuitem_about.connect("activate", self.on_about, "activate")
        menuitem_about.add_accelerator("activate",
                                       accelgroup,
                                       Gdk.keyval_from_name("h"),
                                       Gdk.ModifierType.CONTROL_MASK,
                                       Gtk.AccelFlags.VISIBLE)
        help_menu.append(menuitem_about)

        menubar.append(file_menuitem)
        menubar.append(play_menuitem)
        menubar.append(edit_menuitem)
        menubar.append(help_menuitem)

        # /MENU

        main_window.connect("key-press-event", self.on_key_event, file_menu)

        video_window.connect("button-press-event", self.on_event, file_menu)
        # Playlist
        
        self.playlist_window = Gtk.ScrolledWindow()

        self.playlist_window.set_hexpand(False)
        self.playlist_window.set_vexpand(False)

        self.playlist_window.set_min_content_height(200)
        
        self.liststore = Gtk.ListStore(int, str, str)

        for i in range(len(self.dummyArray)):
            if i % 2 == 0:
                background_color = "#232629"
                self.liststore.append(self.dummyArray[i] + (background_color,))
            else:
                background_color = "#31363b"
                self.liststore.append(self.dummyArray[i] + (background_color,))
                
        self.treeview = Gtk.TreeView(model=self.liststore)
        
        selected_playlist_item = self.treeview.get_selection()
        # selected_playlist_item.connect("changed", self.on_selected_playlist_item)

        self.treeview.connect("row-activated", self.on_selected_playlist_item)

        self.treeview.set_activate_on_single_click(False)
        
        self.playlist_window.add(self.treeview)
        treeviewcolumn = Gtk.TreeViewColumn("Order")
        self.treeview.append_column(treeviewcolumn)
        cellrenderertext = Gtk.CellRendererText()
        treeviewcolumn.pack_start(cellrenderertext, True)
        treeviewcolumn.add_attribute(cellrenderertext, "text", 0)
        treeviewcolumn.add_attribute(cellrenderertext, "background", 2)

        treeviewcolumn = Gtk.TreeViewColumn("Name")
        self.treeview.append_column(treeviewcolumn)
        cellrenderertext = Gtk.CellRendererText()
        treeviewcolumn.pack_start(cellrenderertext, True)
        treeviewcolumn.add_attribute(cellrenderertext, "text", 1)
        treeviewcolumn.add_attribute(cellrenderertext, "background", 2)

        # /Playlist
        
        iconSize = Gtk.IconSize.LARGE_TOOLBAR

        loadIcon = Gtk.Image.new_from_icon_name("edit-cut", iconSize)
        playIcon = Gtk.Image.new_from_icon_name("media-playback-start", iconSize)
        pauseIcon = Gtk.Image.new_from_icon_name("media-playback-pause", iconSize)
        stopIcon = Gtk.Image.new_from_icon_name("media-playback-stop", iconSize)

        cut_button = Gtk.ToolButton(icon_widget=loadIcon, label="Load")
        cut_button.set_can_focus(False)
        cut_button.connect("clicked", self.on_cut)


        
        self.play_button_image_play = Gtk.Image()
        self.play_button_image_play.set_from_icon_name(Gtk.STOCK_MEDIA_PLAY, Gtk.IconSize.BUTTON)

        self.play_button_image_pause = Gtk.Image()
        self.play_button_image_pause.set_from_icon_name(Gtk.STOCK_MEDIA_PAUSE, Gtk.IconSize.BUTTON)

        stop_button_image = Gtk.Image()
        stop_button_image.set_from_icon_name(Gtk.STOCK_MEDIA_STOP, Gtk.IconSize.BUTTON)

        
        self.play_button = Gtk.Button()
        self.play_button.set_image(self.play_button_image_pause)
        self.play_button.set_can_focus(False)
        # gtk_widget_set_can_focus(play_button, False)
        # play_button.unset_flags(Gtk.CAN_FOCUS)
        self.play_button.connect("clicked", self.on_toggle_play_event, 'activate')

        # pause_button = Gtk.Button.new_from_stock(Gtk.STOCK_MEDIA_PAUSE)
        pause_button = Gtk.Button(label="Pause")
        pause_button.set_can_focus(False)
        pause_button.connect("clicked", self.on_pause)

        # stop_button = Gtk.Button.new_from_stock(Gtk.STOCK_MEDIA_STOP)
        stop_button = Gtk.Button()
        stop_button.set_image(stop_button_image)
        stop_button.set_can_focus(False)
        stop_button.connect("clicked", self.on_stop)

        self.volume_button = Gtk.VolumeButton()
        self.volume_button.connect("value-changed", self.on_volume)
        
        self.slider = Gtk.Scale.new_with_range(orientation=Gtk.Orientation.HORIZONTAL, min=0, max=100, step=1)
        self.slider.set_can_focus(False)
        self.slider.set_draw_value(True)
        self.slider_update_signal_id = self.slider.connect("value-changed", self.on_slider_changed)

        self.start_slider = Gtk.Scale.new_with_range(orientation=Gtk.Orientation.HORIZONTAL, min=0, max=100, step=1)
        self.end_slider = Gtk.Scale.new_with_range(orientation=Gtk.Orientation.HORIZONTAL, min=0, max=100, step=1)
        
        self.streams_list = Gtk.TextView.new()
        self.streams_list.set_editable(False)

        controls = Gtk.Box(spacing=5, orientation=Gtk.Orientation.HORIZONTAL)
        controls.pack_start(self.play_button, False, False, 2)
        controls.pack_start(stop_button, False, False, 2)
        controls.pack_start(self.volume_button, False, False, 2)
        # controls.pack_start(cut_button, True, False, 2)

        seek_slider = Gtk.Box(spacing=5, orientation=Gtk.Orientation.VERTICAL)
        seek_slider.pack_start(self.slider, True, False, 0)
        
        self.cut_sliders = Gtk.Box(spacing=5, orientation=Gtk.Orientation.VERTICAL)
        self.cut_sliders.pack_start(self.start_slider, True, False, 0)
        self.cut_sliders.pack_start(self.end_slider, True, False, 0)


        main_hbox = Gtk.Box(spacing=5, orientation=Gtk.Orientation.HORIZONTAL)
        main_hbox.pack_start(video_window, True, True, 0)
        main_hbox.pack_start(self.streams_list, False, False, 2)

        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)
        main_box.add(grid)
        main_box.pack_start(main_hbox, True, True, 0)
        main_box.pack_start(seek_slider, False, False, 0)
        main_box.pack_start(self.playlist_window, False, True, 0)
        main_box.pack_start(self.cut_sliders, False, False, 0)
        main_box.pack_start(controls, False, False, 0)

        # main_window.add(grid)        
        main_window.add(main_box)
        main_window.set_default_size(640, 480)
        main_window.show_all()
        self.cut_sliders.hide()
        self.streams_list.hide()
        self.playlist_window.hide()
        
    def on_item_activated(self, recent_chooser_menu):
        item = recent_chooser_menu.get_current_item()

        if item:
            print("Item selected:")
            print("Name:\t %s" % (item.get_display_name()))
            print("URI:\t %s" % (item.get_uri()))
            

    def on_key_event(self, widget, event, menu):

        print("EVENT: ", event.keyval)

        if event.keyval == 65361:
            self.on_seek(self, "left", self.seconds_to_seek)
        elif event.keyval == 65363:
            print("right")
            self.on_seek(self, "right", self.seconds_to_seek)
        elif event.keyval == 65366:
            print("Big left")
            self.on_seek(self, "left", 100)
        elif event.keyval == 65365:
            print("Big right")
            self.on_seek(self, "right", 100)

        pass
    # return True
        
    def on_event(self, widget, event, menu):
        if event.button == 1:
            if self.state == Gst.State.PLAYING:
                self.playbin.set_state(Gst.State.PAUSED)
            elif self.state == Gst.State.PAUSED:
                self.playbin.set_state(Gst.State.PLAYING)

        if event.button == 3:
            menu.popup(None, None, None, None, event.button, event.time)
            menu.show_all()

        return True
    
        
    # this function is called when the GUI toolkit creates the physical window
    # that will hold the video
    # at this point we can retrieve its handler and pass it to GStreamer
    # through the XOverlay interface
    def on_realize(self, widget):
        window = widget.get_window()
        window_handle = window.get_xid()

        # pass it to playbin, which implements XOverlay and will forward
        # it to the video sink
        self.playbin.set_window_handle(window_handle)
        # self.playbin.set_xwindow_id(window_handle)
        
    # this function is called when the LOAD / EJECT button is clicked
    def on_file_open(self, widget):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=None, action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL,
            Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN,
            Gtk.ResponseType.OK
        )

        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.playbin.set_state(Gst.State.PAUSED)
            self.playbin.set_state(Gst.State.READY)
            self.playbin.set_property("uri", "file://" + dialog.get_filename())
            self.playbin.set_state(Gst.State.PLAYING)
            print("Open clicked")
            print("File selected: " + dialog.get_filename())
        elif response == Gtk.ResponseType.CANCEL:
            print("Cancel clicked")

        dialog.destroy()
        
    # this function is called when the PLAY button is clicked
    def on_cut(self, button):
        value_start = self.start_slider.get_value()
        value_end = self.end_slider.get_value()
        print("cut!:", value_start, value_end, self.playbin.get_property("current-uri"))

        # asset = GES.UriClipAsset.request_sync(sys.argv[1])
        # audio_asset = GES.UriClipAsset.request_sync(sys.argv[3])

        # timeline = GES.Timeline.new_from_uri(self.playbin.get_property("current-uri"))

        # print("PROP:", self.playbin.get_property("current-uri"))

    def combo_changed(self, widget):
        row = 1  # the ID of the requested row
        print(row)
        self.treeview.row_activated(Gtk.TreePath(row), Gtk.TreeViewColumn(None))
        self.treeview.set_cursor(Gtk.TreePath(row))
        
        return True
    
    # this function is called when the PLAY button is clicked
    def on_play(self, button):
        button.set_label('plop')
        self.playbin.set_state(Gst.State.PLAYING)
        pass

    def on_toggle_play(self, button):
        if self.state == Gst.State.PLAYING:
            button.set_label('Play')
            self.playbin.set_state(Gst.State.PAUSED)
        elif self.state == Gst.State.PAUSED:
            button.set_label('Pause')
            self.playbin.set_state(Gst.State.PLAYING)
        pass

    # this function is called when the PAUSE button is clicked
    def on_pause(self, button):
        self.playbin.set_state(Gst.State.PAUSED)
        pass

    # this function is called when the STOP button is clicked
    def on_stop(self, button):
        self.playbin.set_state(Gst.State.READY)
        self.slider.set_value(0)
        pass

    # this function is called when the main window is closed
    def on_delete_event(self, widget, event):
        self.on_stop(None)
        Gtk.main_quit()

    # this function is called when the space bar is pressed
    def on_toggle_play_event(self, widget, event):
        if self.state == Gst.State.PLAYING:
            self.playbin.set_state(Gst.State.PAUSED)
        elif self.state == Gst.State.PAUSED:
            self.playbin.set_state(Gst.State.PLAYING)

    def on_toggle_playlist(self, widget, event):
        if widget.get_active():
            self.playlist_window.show()
        else:
            self.playlist_window.hide()
        return True

    def on_properties(self, widget, event):
        if widget.get_active():
            self.streams_list.show()
        else:
            self.streams_list.hide()
        return True

    # this function is called when the trim func is called
    def on_edit_menu_trim(self, widget, event):
        if widget.get_active():
            self.cut_sliders.show()
        else:
            self.cut_sliders.hide()
        return True

    # this function is called when the trim func is called
    def on_about(self, widget, event):

        def on_response(self, widget, event):
            self.hide()
            return True
        
        about_dialog = Gtk.AboutDialog.new()
        
        about_dialog.set_title("Fwomaj")
        about_dialog.set_name("yPhil")
        about_dialog.set_version("1.0")
        about_dialog.set_comments("Cool media player")
        about_dialog.set_website("https://yphil.gitlab.io/blog/")
        about_dialog.set_website_label("yPhil Website")
        about_dialog.set_authors(["yPhil"])
        # about_dialog.set_logo(logo)
        about_dialog.connect("response", on_response, "response")

        about_dialog.show()
        
    # this function is called every time the video window needs to be
    # redrawn. GStreamer takes care of this in the PAUSED and PLAYING states.
    # in the other states we simply draw a black rectangle to avoid
    # any garbage showing up
    def on_draw(self, widget, cr):
        if self.state < Gst.State.PAUSED:
            allocation = widget.get_allocation()

            cr.set_source_rgb(0, 0, 0)
            cr.rectangle(0, 0, allocation.width, allocation.height)
            cr.fill()

        return False

    # this function is called when the direction keys are pressed.
    # we perform a seek to the new position here
    def on_seek(self, widget, direction, seconds):

        pos = (self.playbin.query_position(Gst.Format.TIME)[1]) / Gst.SECOND  
        hh_pos = int(pos/3600)
        mm_pos, ss_pos = (divmod(int(divmod(pos,3600)[1]),60))
        time = '{0}:{1}:{2}'.format(hh_pos,mm_pos,ss_pos)
        date_time = datetime.datetime.strptime(time, "%H:%M:%S")
        a_timedelta = date_time - datetime.datetime(1900, 1, 1)
        total_seconds = a_timedelta.total_seconds()

        duration = (self.playbin.query_duration(Gst.Format.TIME)[1]) / Gst.SECOND
        
        if direction == "right":
            seek_time = total_seconds + seconds
        else:
            seek_time = total_seconds - seconds

        if seek_time > duration:
            seek_time = duration -1

        self.playbin.seek(1.00, 
                          Gst.Format.TIME,
                          Gst.SeekFlags.FLUSH | Gst.SeekFlags.TRICKMODE,
                          Gst.SeekType.SET,
                          seek_time * Gst.SECOND,
                          Gst.SeekType.NONE,
                          -1)

        # self.playbin.seek_simple(Gst.Format.TIME, Gst.SeekFlags.FLUSH, seek_time * Gst.SECOND)

    # this function is called when the slider changes its position.
    # we perform a seek to the new position here
    def on_slider_changed(self, range):
        value = self.slider.get_value()
        
        pos = (self.playbin.query_position(Gst.Format.TIME)[1]) / Gst.SECOND  

        hh_pos = int(value/3600)
        mm_pos, ss_pos = (divmod(int(divmod(value,3600)[1]),60))
        
        # self.playbin.seek_simple(Gst.Format.TIME,
        #                          Gst.SeekFlags.FLUSH | Gst.SeekFlags.KEY_UNIT,
        #                          value * Gst.SECOND)

        self.playbin.seek(1.00, 
                          Gst.Format.TIME,
                          (Gst.SeekFlags.FLUSH | Gst.SeekFlags.TRICKMODE),
                          Gst.SeekType.SET,
                          value * Gst.SECOND,
                          Gst.SeekType.END,
                          -1)
        
    def on_volume(self, widget, other):
        value = self.slider.get_value()
        self.playbin.set_volume(GstAudio.StreamVolumeFormat.LINEAR, value)
        # print('value: ', value)
        
    # this function is called periodically to refresh the GUI
    def refresh_ui(self):
        current = -1

        # we do not want to update anything unless we are in the PAUSED
        # or PLAYING states
        if self.state < Gst.State.PAUSED:
            return True
        # else:
            # print("Mm, Refreshing!")
        # if we don't know it yet, query the stream duration
        if self.duration == Gst.CLOCK_TIME_NONE:
            ret, self.duration = self.playbin.query_duration(Gst.Format.TIME)
            if not ret:
                print("ERROR: Could not query current duration")
            else:
                # print("Duration: ", self.duration)
                # set the range of the slider to the clip duration (in seconds)
                self.slider.set_range(0, self.duration / Gst.SECOND)

        ret, current = self.playbin.query_position(Gst.Format.TIME)
        if ret:
            # block the "value-changed" signal, so the on_slider_changed
            # callback is not called (which would trigger a seek the user
            # has not requested)
            
            self.slider.handler_block(self.slider_update_signal_id)

            # set the position of the slider to the current pipeline position
            # (in seconds)
            self.slider.set_value(current / Gst.SECOND)
            
            # enable the signal again
            self.slider.handler_unblock(self.slider_update_signal_id)

        return True

    # this function is called when new metadata is discovered in the stream
    def on_tags_changed(self, playbin, stream):
        # we are possibly in a GStreamer working thread, so we notify
        # the main thread of this event through a message in the bus
        self.playbin.post_message(
            Gst.Message.new_application(
                self.playbin,
                Gst.Structure.new_empty("tags-changed")))

    # this function is called when an error message is posted on the bus
    def on_error(self, bus, msg):
        err, dbg = msg.parse_error()
        print("ERROR:", msg.src.get_name(), ":", err.message)
        if dbg:
            print("Debug info:", dbg)

    def on_selected_playlist_item(self, tree_view, path, column):
        print('on_selected_playlist_item')
        # print("tree_view {0} path {1} column {2}".format(tree_view, path, column))
        self.playbin.set_state(Gst.State.PAUSED)
        self.playbin.set_state(Gst.State.READY)
        self.playbin.set_property("uri", "file://" + self.liststore[path][1])
        self.duration = Gst.CLOCK_TIME_NONE
        self.playbin.set_state(Gst.State.PLAYING)
        # pass

    # this function is called when an End-Of-Stream message is posted on the bus
    # we just set the pipeline to READY (which stops playback)
    def on_eos(self, bus, msg):
        just_ended_uri = self.playbin.get_property("current-uri")
        print("End-Of-Stream reached:", "(" + just_ended_uri + ")")

        nb_items = 0
        
        
        rootiter = self.liststore.get_iter_first()
        
        for row in self.liststore:
            nb_items = nb_items + 1
            if "file://" + row[:][1] == just_ended_uri:
                print("Kayn!")
                just_ended_id = row[:][0]

        print("nb_items:", nb_items)
        row = just_ended_id  # the ID of the requested row
        print(row)
        next_row = just_ended_id + 1  # the ID of the requested row

        if next_row < nb_items:
            self.treeview.row_activated(Gtk.TreePath(next_row), Gtk.TreeViewColumn(None))
            self.treeview.set_cursor(Gtk.TreePath(next_row))
        
        # child_id = self.treeview.get_children()[-1]

        # print("treeview.get_children: ", self.treeview.get_children())
        
        # self.treeview.focus(child_id)
        # self.treeview.selection_set(child_id)

    def SearchTreeRows(self, store, treeiter, searchstr):
        print("\nsearch>%s"%searchstr)
        while treeiter != None:

            if store[treeiter][2] ==searchstr:
                print("found in:%s"%str(store[treeiter][:]))
                return(treeiter)
                break
            print("searched:%s"%str(store[treeiter][:]))    
            if store.iter_has_child(treeiter):
                childiter = store.iter_children(treeiter)
                ret = self.SearchTreeRows(store, childiter, searchstr)
                if ret is not None:
                    return ret

            treeiter = store.iter_next(treeiter)

    def NodeId2Tree(self,nodeid):
        self.store = self.builder.get_object('theTreeStore')
        rootiter = self.store.get_iter_first()
        row = self.SearchTreeRows(self.store, rootiter,nodeid)
        return(row)
    
    # this function is called when the pipeline changes states.
    # we use it to keep track of the current state
    def on_state_changed(self, bus, msg):
        old, new, pending = msg.parse_state_changed()
        if not msg.src == self.playbin:
            # not from the playbin, ignore
            return

        self.state = new
        print("State changed from {0} to {1}".format(
            Gst.Element.state_get_name(old), Gst.Element.state_get_name(new)))


        self.play_button.set_image(self.play_button_image_pause)

        if new == Gst.State.PAUSED or new == Gst.State.READY:
            self.play_button.set_image(self.play_button_image_play)

        # if old == Gst.State.READY and new == Gst.State.PAUSED:
        #     # for extra responsiveness we refresh the GUI as soons as
        #     # we reach the PAUSED state
        #     self.refresh_ui()

    # extract metadata from all the streams and write it to the text widget
    # in the GUI
    def analyze_streams(self):
        # clear current contents of the widget
        buffer = self.streams_list.get_buffer()
        buffer.set_text("")

        # read some properties
        nr_video = self.playbin.get_property("n-video")
        nr_audio = self.playbin.get_property("n-audio")
        nr_text = self.playbin.get_property("n-text")

        for i in range(nr_video):
            tags = None
            # retrieve the stream's video tags
            tags = self.playbin.emit("get-video-tags", i)
            if tags:
                buffer.insert_at_cursor("video stream {0}\n".format(i))
                _, str = tags.get_string(Gst.TAG_VIDEO_CODEC)
                buffer.insert_at_cursor(
                    "  codec: {0}\n".format(
                        str or "unknown"))

        for i in range(nr_audio):
            tags = None
            # retrieve the stream's audio tags
            tags = self.playbin.emit("get-audio-tags", i)
            if tags:
                buffer.insert_at_cursor("\naudio stream {0}\n".format(i))
                ret, str = tags.get_string(Gst.TAG_AUDIO_CODEC)
                if ret:
                    buffer.insert_at_cursor(
                        "  codec: {0}\n".format(
                            str or "unknown"))

                ret, str = tags.get_string(Gst.TAG_LANGUAGE_CODE)
                if ret:
                    buffer.insert_at_cursor(
                        "  language: {0}\n".format(
                            str or "unknown"))

                ret, str = tags.get_uint(Gst.TAG_BITRATE)
                if ret:
                    buffer.insert_at_cursor(
                        "  bitrate: {0}\n".format(
                            str or "unknown"))

        for i in range(nr_text):
            tags = None
            # retrieve the stream's subtitle tags
            tags = self.playbin.emit("get-text-tags", i)
            if tags:
                buffer.insert_at_cursor("\nsubtitle stream {0}\n".format(i))
                ret, str = tags.get_string(Gst.TAG_LANGUAGE_CODE)
                if ret:
                    buffer.insert_at_cursor(
                        "  language: {0}\n".format(
                            str or "unknown"))

    # this function is called when an "application" message is posted on the bus
    # here we retrieve the message posted by the on_tags_changed callback
    def on_application_message(self, bus, msg):
        print("on_application_message")
        if msg.get_structure().get_name() == "tags-changed":
            # if the message is the "tags-changed", update the stream info in
            # the GUI
            self.analyze_streams()

    def on_info(self, widget):
        """
        File info menu callback. A GTK message window.
        """

        dialog = Gtk.MessageDialog(self, 0, Gtk.MessageType.INFO,
                                   Gtk.ButtonsType.OK, "plop")

        dialog.set_default_size(500, 800)
        box = dialog.get_message_area()

        liststore = Gtk.ListStore(str, str)
        treeview = Gtk.TreeView(model=liststore)

        scrolledwindow = Gtk.ScrolledWindow()
        scrolledwindow.set_policy(Gtk.PolicyType.NEVER, Gtk.PolicyType.AUTOMATIC)
        scrolledwindow.set_size_request(500, 800)

        renderer_1 = Gtk.CellRendererText()
        renderer_2 = Gtk.CellRendererText()
        column_1 = Gtk.TreeViewColumn("Parameter", renderer_1, text=0)
        column_2 = Gtk.TreeViewColumn("Value", renderer_2, text=1)

        treeview.append_column(column_1)
        treeview.append_column(column_2)

        for tag in ("plop", "plip", "ploop").split('\n'):
            line = tag.partition(":")
            liststore.append([line[0], line[2][:50]])

        scrolledwindow.add_with_viewport(treeview)

        box.add(scrolledwindow)

        box.show_all()
        dialog.run()
        dialog.destroy()

            
if __name__ == '__main__':
    p = Fwomaj()
    p.start()
