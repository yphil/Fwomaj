#!/usr/bin/env python3

import os
import sys
import gi
gi.require_version('Gtk', '4.0')
gi.require_version('Adw', '1')
from gi.repository import Gtk, Adw, Gio, GLib, Gdk
# from gi.repository import GdkPixbuf

css = b"""
        .scale trough {
          background-color: #00ff00;
        }

        .scale slider {
          border-width: 3px;
          border-style: solid;
          border-radius: 10px;
          border-color: transparent;
          background-clip: padding-box;
          background-color: #999;
        }

        .scale slider:hover {
          background-color: #00ffff;
        }
        """


class MainWindow(Gtk.ApplicationWindow):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        style_provider = Gtk.CssProvider()
        style_provider.load_from_data(css)
        Gtk.StyleContext.add_provider_for_display(Gdk.Display().get_default(),
                                                  style_provider,
                                                  Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)

        self.header = Gtk.HeaderBar()
        self.set_titlebar(self.header)

        self.set_default_size(600, 250)
        self.set_title("Fwomaj")

        icon_theme = Gtk.IconTheme.get_for_display(Gdk.Display.get_default())
        Gtk.IconTheme.set_search_path(icon_theme, "./")

        self.set_icon_name("fwomaj")
        self.set_default_icon_name("fwomaj")

        action = Gio.SimpleAction.new("open", None)
        action.connect("activate", self.show_open_dialog)
        self.add_action(action)

        action = Gio.SimpleAction.new("about", None)
        action.connect("activate", self.on_about)
        self.add_action(action)

        action = Gio.SimpleAction.new("quit", None)
        action.connect("activate", app.on_quit)
        self.add_action(action)

        menu = Gio.Menu.new()
        menu.append("_Open", "win.open")
        menu.append("_About", "win.about")
        menu.append("_Quit", "win.quit")

        self.popover = Gtk.PopoverMenu()  # Create a new popover menu
        self.popover.set_menu_model(menu)

        self.hamburger = Gtk.MenuButton()
        self.hamburger.set_popover(self.popover)
        self.hamburger.set_icon_name("open-menu-symbolic")  # Give it a nice icon

        self.header.pack_start(self.hamburger)

        self.box1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL)

        self.video = Gtk.Video()

        self.load_video_file(app.loaded_file)

        # self.media_stream =  Gtk.MediaFile.new_for_filename("/home/px/src/Fwomaj/tmp/examples_data_sintel-trailer-480p.webm")
        # self.video = Gtk.Video.new_for_media_stream(self.media_stream)
        self.video.set_autoplay(False)
        self.video.set_hexpand(True)
        self.video.set_vexpand(True)

        self.box1.append(self.video)

        self.set_child(self.box1)

        self.button = Gtk.Button(label="Hello")
        self.button.connect('clicked', self.hello, kwargs)
        self.open_dialog = Gtk.FileChooserNative.new(title="Choose a file",
                                                     parent=self, action=Gtk.FileChooserAction.OPEN)

        self.open_dialog.connect("response", self.open_response)
        self.slider = Gtk.Scale()
        self.slider.get_style_context().add_class("scale")
        self.slider.set_digits(0)  # Number of decimal places to use
        self.slider.set_range(0, 10)
        self.slider.set_draw_value(True)
        self.slider.set_value(0)
        self.slider.connect('value-changed', self.slider_changed)
        self.box1.append(self.slider)

    def load_css(self, css_fn):
        """create a provider for custom styling"""
        if css_fn and os.path.exists(css_fn):
            css_provider = Gtk.CssProvider()
            try:
                css_provider.load_from_path(css_fn)
            except GLib.Error as e:
                print(f"Error loading CSS : {e} ")
                return None
            print(f'loading custom styling : {css_fn}')
            self.css_provider = css_provider

    def show_about(self, action, param):
        self.about = Gtk.AboutDialog()
        self.about.set_transient_for(self)  # Makes the dialog always appear in from of the parent window
        self.about.set_modal(self)  # Makes the parent window unresponsive while dialog is showing

        self.about.set_authors(["Your Name"])
        self.about.set_copyright("Copyright 2022 Your Full Name")
        self.about.set_license_type(Gtk.License.GPL_3_0)
        self.about.set_website("http://example.com")
        self.about.set_website_label("My Website")
        self.about.set_version("1.0")
        self.about.set_logo_icon_name("org.example.example")  # The icon will need to be added to appropriate location
        # E.g. /usr/share/icons/hicolor/scalable/apps/org.example.example.svg

        self.about.show()

    def load_video_file(self, file_name):
        self.video.set_media_stream(Gtk.MediaFile.new_for_filename(file_name))

    def slider_changed(self, slider):
        print(int(slider.get_value()))

    def show_open_dialog(self, widget, other):
        self.open_dialog.show()

    def open_response(self, dialog, response):
        if response == Gtk.ResponseType.ACCEPT:
            self.load_video_file(dialog.get_file().get_path())

    def hello(self, button, args):
        print("Args: ", args)

    def create_action(self, name, callback):
        """ Add an Action and connect to a callback """
        action = Gio.SimpleAction.new(name, None)
        action.connect("activate", callback)
        self.add_action(action)

    def menu_handler(self, action, state):
        """ Callback for  menu actions"""
        name = action.get_name()
        print(f'active : {name}')
        if name == 'quit':
            self.close()
        elif name == 'shortcuts':
            self.show_shortcuts()

    def on_about(self, action, param):
        # about_dialog = Gtk.AboutDialog(modal=True)
        self.about = Gtk.AboutDialog()
        self.about.set_transient_for(self)  # Makes the dialog always appear in from of the parent window
        self.about.set_modal(self)  # Makes the parent window unresponsive while dialog is showing

        self.about.set_authors(["Your Name"])
        self.about.set_copyright("Copyright 2022 Your Full Name")
        self.about.set_license_type(Gtk.License.GPL_3_0)
        self.about.set_website("http://example.com")
        self.about.set_website_label("My Website")
        self.about.set_version("1.0")
        # self.about.set_logo_icon_name("/home/px/src/Fwomaj/fwomaj.png")  # The icon will need to be added to appropriate location

        self.about.set_logo_icon_name("fwomaj")  # Give it a nice icon

        self.about.present()


class MyApp(Adw.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="org.example.myapp",
            flags=Gio.ApplicationFlags.HANDLES_COMMAND_LINE | Gio.ApplicationFlags.HANDLES_OPEN,
            **kwargs
        )
        self.connect('activate', self.on_activate)

        self.add_main_option(
            "test",
            ord("t"),
            GLib.OptionFlags.NONE,
            GLib.OptionArg.NONE,
            "Command line test",
            None,
        )

        self.add_main_option(
            "file",
            0,
            GLib.OptionFlags.NONE,
            GLib.OptionArg.FILENAME,
            "Video file to load",
            None,
        )

    def on_activate(self, app):
        self.win = MainWindow(application=app)
        self.win.present()

    def on_quit(self, action, param):
        self.quit()

    def do_open(self):
        print("Now what")

    def do_command_line(self, command_line):
        options = command_line.get_options_dict()
        # convert GVariantDict -> GVariant -> dict
        options = options.end().unpack()

        argv = command_line.get_arguments()
        print("options: ", argv)

        if "test" in options:
            print("Test argument recieved: %s" % options["test"])

        if "file" in options:
            print("File argument received: %s" % options["file"])
            # string = options["file"].decode('utf-8')
            print("type:", ''.join(options["file"]).decode('utf-8'))

        self.loaded_file = b"{options['file']}"

        self.activate()
        return 0


app = MyApp()
app.run(sys.argv)
